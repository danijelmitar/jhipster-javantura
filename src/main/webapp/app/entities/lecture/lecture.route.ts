import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { LectureComponent } from './lecture.component';
import { LectureDetailComponent } from './lecture-detail.component';
import { LecturePopupComponent } from './lecture-dialog.component';
import { LectureDeletePopupComponent } from './lecture-delete-dialog.component';

export const lectureRoute: Routes = [
    {
        path: 'lecture',
        component: LectureComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Lectures'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'lecture/:id',
        component: LectureDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Lectures'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const lecturePopupRoute: Routes = [
    {
        path: 'lecture-new',
        component: LecturePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Lectures'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'lecture/:id/edit',
        component: LecturePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Lectures'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'lecture/:id/delete',
        component: LectureDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Lectures'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
