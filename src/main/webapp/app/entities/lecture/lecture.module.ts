import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { JavanturaSharedModule } from '../../shared';
import {
    LectureService,
    LecturePopupService,
    LectureComponent,
    LectureDetailComponent,
    LectureDialogComponent,
    LecturePopupComponent,
    LectureDeletePopupComponent,
    LectureDeleteDialogComponent,
    lectureRoute,
    lecturePopupRoute,
} from './';

const ENTITY_STATES = [
    ...lectureRoute,
    ...lecturePopupRoute,
];

@NgModule({
    imports: [
        JavanturaSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        LectureComponent,
        LectureDetailComponent,
        LectureDialogComponent,
        LectureDeleteDialogComponent,
        LecturePopupComponent,
        LectureDeletePopupComponent,
    ],
    entryComponents: [
        LectureComponent,
        LectureDialogComponent,
        LecturePopupComponent,
        LectureDeleteDialogComponent,
        LectureDeletePopupComponent,
    ],
    providers: [
        LectureService,
        LecturePopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class JavanturaLectureModule {}
