import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { Lecture } from './lecture.model';
import { LecturePopupService } from './lecture-popup.service';
import { LectureService } from './lecture.service';

@Component({
    selector: 'jhi-lecture-delete-dialog',
    templateUrl: './lecture-delete-dialog.component.html'
})
export class LectureDeleteDialogComponent {

    lecture: Lecture;

    constructor(
        private lectureService: LectureService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.lectureService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'lectureListModification',
                content: 'Deleted an lecture'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-lecture-delete-popup',
    template: ''
})
export class LectureDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private lecturePopupService: LecturePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.lecturePopupService
                .open(LectureDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
