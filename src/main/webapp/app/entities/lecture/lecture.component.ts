import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiAlertService } from 'ng-jhipster';

import { Lecture } from './lecture.model';
import { LectureService } from './lecture.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-lecture',
    templateUrl: './lecture.component.html'
})
export class LectureComponent implements OnInit, OnDestroy {
lectures: Lecture[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private lectureService: LectureService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {
    }

    loadAll() {
        this.lectureService.query().subscribe(
            (res: ResponseWrapper) => {
                this.lectures = res.json;
            },
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }
    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInLectures();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: Lecture) {
        return item.id;
    }
    registerChangeInLectures() {
        this.eventSubscriber = this.eventManager.subscribe('lectureListModification', (response) => this.loadAll());
    }

    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }
}
