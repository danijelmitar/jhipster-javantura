import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { Lecture } from './lecture.model';
import { LecturePopupService } from './lecture-popup.service';
import { LectureService } from './lecture.service';
import { Category, CategoryService } from '../category';
import { Speaker, SpeakerService } from '../speaker';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-lecture-dialog',
    templateUrl: './lecture-dialog.component.html'
})
export class LectureDialogComponent implements OnInit {

    lecture: Lecture;
    isSaving: boolean;

    categories: Category[];

    speakers: Speaker[];

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private lectureService: LectureService,
        private categoryService: CategoryService,
        private speakerService: SpeakerService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.categoryService.query()
            .subscribe((res: ResponseWrapper) => { this.categories = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.speakerService.query()
            .subscribe((res: ResponseWrapper) => { this.speakers = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.lecture.id !== undefined) {
            this.subscribeToSaveResponse(
                this.lectureService.update(this.lecture));
        } else {
            this.subscribeToSaveResponse(
                this.lectureService.create(this.lecture));
        }
    }

    private subscribeToSaveResponse(result: Observable<Lecture>) {
        result.subscribe((res: Lecture) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: Lecture) {
        this.eventManager.broadcast({ name: 'lectureListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackCategoryById(index: number, item: Category) {
        return item.id;
    }

    trackSpeakerById(index: number, item: Speaker) {
        return item.id;
    }

    getSelected(selectedVals: Array<any>, option: any) {
        if (selectedVals) {
            for (let i = 0; i < selectedVals.length; i++) {
                if (option.id === selectedVals[i].id) {
                    return selectedVals[i];
                }
            }
        }
        return option;
    }
}

@Component({
    selector: 'jhi-lecture-popup',
    template: ''
})
export class LecturePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private lecturePopupService: LecturePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.lecturePopupService
                    .open(LectureDialogComponent as Component, params['id']);
            } else {
                this.lecturePopupService
                    .open(LectureDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
