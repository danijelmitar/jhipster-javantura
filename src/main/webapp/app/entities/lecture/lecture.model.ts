import { BaseEntity } from './../../shared';

export const enum LectureType {
    'PRESENTATION',
    'PRESENTATION_WITH_DEMO'
}

export const enum LectureLevel {
    'GENERAL',
    'VERY_DETAILED',
    'SOMETHING_IN_BETWEEN'
}

export class Lecture implements BaseEntity {
    constructor(
        public id?: number,
        public name?: string,
        public description?: string,
        public lectureType?: LectureType,
        public lectureLevel?: LectureLevel,
        public category?: BaseEntity,
        public speakers?: BaseEntity[],
    ) {
    }
}
