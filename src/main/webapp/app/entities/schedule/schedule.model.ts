import { BaseEntity } from './../../shared';

export const enum Track {
    'HALL_A',
    'HALL_B',
    'HALL_C'
}

export class Schedule implements BaseEntity {
    constructor(
        public id?: number,
        public startTime?: any,
        public endTime?: any,
        public track?: Track,
        public lecture?: BaseEntity,
    ) {
    }
}
