import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { JavanturaCategoryModule } from './category/category.module';
import { JavanturaLectureModule } from './lecture/lecture.module';
import { JavanturaSpeakerModule } from './speaker/speaker.module';
import { JavanturaScheduleModule } from './schedule/schedule.module';
/* jhipster-needle-add-entity-module-import - JHipster will add entity modules imports here */

@NgModule({
    imports: [
        JavanturaCategoryModule,
        JavanturaLectureModule,
        JavanturaSpeakerModule,
        JavanturaScheduleModule,
        /* jhipster-needle-add-entity-module - JHipster will add entity modules here */
    ],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class JavanturaEntityModule {}
