import { BaseEntity } from './../../shared';

export class Speaker implements BaseEntity {
    constructor(
        public id?: number,
        public name?: string,
        public biography?: string,
        public lectures?: BaseEntity[],
    ) {
    }
}
