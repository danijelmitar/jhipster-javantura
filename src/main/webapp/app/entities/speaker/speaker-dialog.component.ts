import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { Speaker } from './speaker.model';
import { SpeakerPopupService } from './speaker-popup.service';
import { SpeakerService } from './speaker.service';
import { Lecture, LectureService } from '../lecture';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-speaker-dialog',
    templateUrl: './speaker-dialog.component.html'
})
export class SpeakerDialogComponent implements OnInit {

    speaker: Speaker;
    isSaving: boolean;

    lectures: Lecture[];

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private speakerService: SpeakerService,
        private lectureService: LectureService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.lectureService.query()
            .subscribe((res: ResponseWrapper) => { this.lectures = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.speaker.id !== undefined) {
            this.subscribeToSaveResponse(
                this.speakerService.update(this.speaker));
        } else {
            this.subscribeToSaveResponse(
                this.speakerService.create(this.speaker));
        }
    }

    private subscribeToSaveResponse(result: Observable<Speaker>) {
        result.subscribe((res: Speaker) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: Speaker) {
        this.eventManager.broadcast({ name: 'speakerListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackLectureById(index: number, item: Lecture) {
        return item.id;
    }

    getSelected(selectedVals: Array<any>, option: any) {
        if (selectedVals) {
            for (let i = 0; i < selectedVals.length; i++) {
                if (option.id === selectedVals[i].id) {
                    return selectedVals[i];
                }
            }
        }
        return option;
    }
}

@Component({
    selector: 'jhi-speaker-popup',
    template: ''
})
export class SpeakerPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private speakerPopupService: SpeakerPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.speakerPopupService
                    .open(SpeakerDialogComponent as Component, params['id']);
            } else {
                this.speakerPopupService
                    .open(SpeakerDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
