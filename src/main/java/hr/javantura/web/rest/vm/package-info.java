/**
 * View Models used by Spring MVC REST controllers.
 */
package hr.javantura.web.rest.vm;
