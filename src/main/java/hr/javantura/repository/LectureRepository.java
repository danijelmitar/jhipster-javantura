package hr.javantura.repository;

import hr.javantura.domain.Lecture;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import java.util.List;

/**
 * Spring Data JPA repository for the Lecture entity.
 */
@SuppressWarnings("unused")
@Repository
public interface LectureRepository extends JpaRepository<Lecture, Long> {
    @Query("select distinct lecture from Lecture lecture left join fetch lecture.speakers")
    List<Lecture> findAllWithEagerRelationships();

    @Query("select lecture from Lecture lecture left join fetch lecture.speakers where lecture.id =:id")
    Lecture findOneWithEagerRelationships(@Param("id") Long id);

}
