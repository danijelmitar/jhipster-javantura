package hr.javantura.domain.enumeration;

/**
 * The LectureType enumeration.
 */
public enum LectureType {
    PRESENTATION, PRESENTATION_WITH_DEMO
}
