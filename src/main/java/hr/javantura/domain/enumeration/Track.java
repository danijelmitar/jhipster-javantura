package hr.javantura.domain.enumeration;

/**
 * The Track enumeration.
 */
public enum Track {
    HALL_A, HALL_B, HALL_C
}
