package hr.javantura.domain.enumeration;

/**
 * The LectureLevel enumeration.
 */
public enum LectureLevel {
    GENERAL, VERY_DETAILED, SOMETHING_IN_BETWEEN
}
