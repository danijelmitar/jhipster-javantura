package hr.javantura.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

import hr.javantura.domain.enumeration.LectureType;

import hr.javantura.domain.enumeration.LectureLevel;

/**
 * A Lecture.
 */
@Entity
@Table(name = "lecture")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Lecture implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "name")
    private String name;

    @Size(max = 1024)
    @Column(name = "description", length = 1024)
    private String description;

    @Enumerated(EnumType.STRING)
    @Column(name = "lecture_type")
    private LectureType lectureType;

    @Enumerated(EnumType.STRING)
    @Column(name = "lecture_level")
    private LectureLevel lectureLevel;

    @ManyToOne
    private Category category;

    @ManyToMany
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "lecture_speaker",
               joinColumns = @JoinColumn(name="lectures_id", referencedColumnName="id"),
               inverseJoinColumns = @JoinColumn(name="speakers_id", referencedColumnName="id"))
    private Set<Speaker> speakers = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public Lecture name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public Lecture description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LectureType getLectureType() {
        return lectureType;
    }

    public Lecture lectureType(LectureType lectureType) {
        this.lectureType = lectureType;
        return this;
    }

    public void setLectureType(LectureType lectureType) {
        this.lectureType = lectureType;
    }

    public LectureLevel getLectureLevel() {
        return lectureLevel;
    }

    public Lecture lectureLevel(LectureLevel lectureLevel) {
        this.lectureLevel = lectureLevel;
        return this;
    }

    public void setLectureLevel(LectureLevel lectureLevel) {
        this.lectureLevel = lectureLevel;
    }

    public Category getCategory() {
        return category;
    }

    public Lecture category(Category category) {
        this.category = category;
        return this;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Set<Speaker> getSpeakers() {
        return speakers;
    }

    public Lecture speakers(Set<Speaker> speakers) {
        this.speakers = speakers;
        return this;
    }

    public Lecture addSpeaker(Speaker speaker) {
        this.speakers.add(speaker);
        speaker.getLectures().add(this);
        return this;
    }

    public Lecture removeSpeaker(Speaker speaker) {
        this.speakers.remove(speaker);
        speaker.getLectures().remove(this);
        return this;
    }

    public void setSpeakers(Set<Speaker> speakers) {
        this.speakers = speakers;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Lecture lecture = (Lecture) o;
        if (lecture.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), lecture.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Lecture{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", description='" + getDescription() + "'" +
            ", lectureType='" + getLectureType() + "'" +
            ", lectureLevel='" + getLectureLevel() + "'" +
            "}";
    }
}
